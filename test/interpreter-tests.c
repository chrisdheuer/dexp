#include "dot.h"
#include "primitives.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main() {
	struct machine_config config;
	config.namesPoolSize = 1024;
	config.descriptionsPoolSize = 1024;
	config.expressionsPoolSize = 1024;
	config.stackSize = 1024;
	config.symbolTableCapacity = 32; // symbol functions is fucked

	struct machine m;
	if (machine_init(&m, &config) != RC_OK) {
		puts("failed to config machine");
		return -1;
	}

	unsigned i = 0;
	for (; i < sizeof(primitives) / sizeof(primitives[0]); ++i) {
		if (symbol_table_add(&m.table, primitives[i].type,
		                               primitives[i].name,
		                               primitives[i].description,
		                               primitives[i].data) != RC_OK)
		{
			puts("failed to add primitives");
			return -1;
		}
	}

	char in_buffer[1024];
	while (!g_quita && fgets(in_buffer, 1024, stdin) != NULL) {
		in_buffer[strcspn(in_buffer, "\n")] = 0;
		machine_eval(&m, in_buffer);
	}


	machine_free(&m);

	return 0;
}
