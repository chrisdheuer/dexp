#include "interpreter.h"

int main(int argc, char **argv) {
	if (interpreter_start(argc, argv) != RC_OK)
		return -1;
	return 0;
}
