CC = gcc
CFLAGS = -O0 -Wall -DLINUX -D_GNU_SOURCE
IFLAGS = -I./include
LIBS = -ldl

BIN_DIR = bin
SRC_DIR = src
TEST_DIR = test

SRC := $(wildcard $(SRC_DIR)/*.c)
TEST := $(patsubst $(TEST_DIR)/%.c, %.c, $(wildcard $(TEST_DIR)/*.c))

.PHONY: $(TEST) run clean

$(TEST): | $(BIN_DIR)
	$(CC) $(CFLAGS) $(IFLAGS) $(TEST_DIR)/$@ $(SRC) $(DEPS) $(LIBS) -o $(BIN_DIR)/$@

$(BIN_DIR):
	mkdir -p $@

run:
	$(BIN_DIR)/$(BIN)

clean:
	rm -rf $(BIN_DIR)
