#include "lolopt.h"
#include <string.h>

char **lolopt(char **opt, char *fmt, int argc, char **argv) {
	char **end = argv + argc;
	switch (*fmt) {
	case '\0':
		if (opt == NULL) opt = argv;
		if (++opt >= end || *(*opt) == '-') return NULL;
		return opt;
	case '-':
		if (fmt[1] == '-') {
			if (opt == NULL) opt = argv;
			size_t len = strlen(fmt);
			if (fmt[len - 1] == '=') {
onearg0:			if (++opt >= end) return NULL;
				if (strncmp(*opt, fmt, len) == 0) return opt;
				goto onearg0;
			} else {
noargs0:			if (++opt >= end) return NULL;
				if (strcmp(*opt, fmt) == 0) return opt;
				goto noargs0;
			}
		} else {
			if (opt == NULL) {
				opt = argv;
anyargs0:			if (++opt >= end) return NULL;
				if (strcmp(*opt, fmt) == 0) goto anyargs1;
				goto anyargs0;
			} else {
anyargs1:			if(++opt >= end) return NULL;
				if (*(*opt) != '-') return opt;
				if (strcmp(*opt, fmt) == 0) goto anyargs1;
				goto anyargs0;
			}
		}
	default:
		return NULL;
	}
}
