#include "interpreter.h"
#include "primitives.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern struct symbol primitives[26] = {{SYMBOL_PRIMITIVE, 0, "print", "pops a string from the stack and prints it", (char*)print},
	                               {SYMBOL_PRIMITIVE, 0, "print-int", "pops an int from the stack and prints it", (char*)print_int},
                                       {SYMBOL_PRIMITIVE, 0, "print-float", "pops a float from the stack and prints it", (char*)print_float},
                                       {SYMBOL_PRIMITIVE, 0, "define", "pops the name, description, and then expression from the stack and tries to save it in the machine", (char*)define},
				       {SYMBOL_PRIMITIVE, 0, "print-stack", "pops everything from the stack and prints it", (char*)print_stack},
				       {SYMBOL_PRIMITIVE, 0, "quit", "quits the interpreter", (char*)quit},
				       {SYMBOL_PRIMITIVE, 0, "print-symbols", "prints all of of the symbols the machine holds", (char*)print_symbols},
				       {SYMBOL_PRIMITIVE, 0, "clear", "clears the screen", (char*)clear},
				       {SYMBOL_PRIMITIVE, 0, "loop", "loops an expression", (char*)loop},
				       {SYMBOL_PRIMITIVE, 0, "notepad", "opens notepad", (char*)notepad},
				       {SYMBOL_PRIMITIVE, 0, "chrome", "pops a string and opens that website in chrome", (char*)chrome},
                                       {SYMBOL_PRIMITIVE, 0, "nil", "does nothing", (char*)nil},
                                       {SYMBOL_PRIMITIVE, 0, "=", "compares", (char*)equals},
                                       {SYMBOL_PRIMITIVE, 0, "+", "adds two numbers", (char*)add},
                                       {SYMBOL_PRIMITIVE, 0, "-", "subtracts two numbers", (char*)sub},
                                       {SYMBOL_PRIMITIVE, 0, "if", "executes an expression and then alternatives if it evaluates to 1 or 0", (char*)_if},
                                       {SYMBOL_PRIMITIVE, 0, "*", "multiplies two numbers", (char*)mult},
                                       {SYMBOL_PRIMITIVE, 0, "eval", "evaluates a string from the top of the stack", (char*)eval},
				       {SYMBOL_PRIMITIVE, 0, "start", "attemps to start a program", (char*)start},
				       {SYMBOL_PRIMITIVE, 0, "change-name", "changes the prompt", (char*)change_name},
				       {SYMBOL_PRIMITIVE, 0, "eval-file", "evaluates a file...", (char*)eval_file},
				       {SYMBOL_PRIMITIVE, 0, "load-library", "loads a shared libary for other function imports", (char*)load_library},
				       {SYMBOL_PRIMITIVE, 0, "load-primitive", "loads a functions from a shared library", (char*)load_primitive},
				       {SYMBOL_PRIMITIVE, 0, "print-libs", "prints all loaded libraries", (char*)print_libs},
				       {SYMBOL_PRIMITIVE, 0, "download" , "asd", (char*)download},
				       {SYMBOL_PRIMITIVE, 0, "print-lib-symbols", "asd", (char*)print_lib_symbols}};


extern bool g_quita = false;

bool print(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;

	if (elt->type != TYPE_STRING)
		return false;

	puts((char*)elt->arr);

	return true;
}

bool print_int(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;

	if (elt->type != TYPE_INT)
		return false;

	printf("%d\n", *(int*)elt->arr);

	return true;
}

bool print_float(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;

	if (elt->type != TYPE_FLOAT)
		return false;

	printf("%.2f\n", *(float*)elt->arr);

	return true;
}

bool define(struct machine *m) {
	char *name;
	char *descr;
	char *expr;

	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;
	name = (char*)elt->arr;


	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;
	descr = (char*)elt->arr;

	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;
	expr = (char*)elt->arr;

	if (machine_add(m, SYMBOL_EXPRESSION, name, descr, expr) != RC_OK) {
		return false;
	}

	return true;
}

bool print_stack(struct machine *m) {
	struct stack_elt *elt;
	while (stack_pop(m->stack, &elt) == RC_OK) {
		switch (elt->type) {
		case TYPE_INT:
			printf("int %d\n", *(int*)elt->arr);
			break;
		case TYPE_STRING:
			printf("string %s\n", (char*)elt->arr);
			break;
		case TYPE_FLOAT:
			printf("float %.2f\n", *(float*)elt->arr);
			break;
		default:
			printf("unknown type\n");
		}
	}

	return true;
}

bool quit(struct machine *m) {
	interpreter_quit();
	return true;
}

bool print_symbols(struct machine *m) {
	unsigned i = 0;
	struct symbol *iter = m->table->symbols;
	for (; i < m->table->count; ++i, ++iter) {
		switch (iter->type) {
		case SYMBOL_PRIMITIVE:
			printf("name: %s\ndescription: %s\nvalue: %p\n\n", iter->name, iter->description, iter->data);
			break;
		case SYMBOL_EXPRESSION:
			printf("name: %s\ndescription: %s\nvalue: %s\n\n", iter->name, iter->description, iter->data);
			break;
		default:
			printf("unknown type ");
		}

	}
	return true;
}

bool loop(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_INT)
		return false;

	int count = *(int*)elt->arr;

	char expr[256];
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;
	strcpy(expr, (char*)elt->arr);

	for (; count > 0; --count) {
		if (machine_eval(m, expr) != RC_OK)
			return false;
	}

	return true;
}

bool nil(struct machine *m) {
	return true;
}

bool equals(struct machine *m) {
	struct stack_elt *e1, *e2;
	if (stack_pop(m->stack, &e1) != RC_OK)
		return false;
	if (stack_pop(m->stack, &e2) != RC_OK)
		return false;

	int result;
	if (e1->type == TYPE_INT && e2->type == TYPE_INT) {
		if (*(int*)e1->arr == *(int*)e2->arr) {
			result = 1;
		}
		else {
			result = 0;
		}
	}
	else if (e1->type == TYPE_STRING && e2->type == TYPE_STRING) {
		if (strcmp((char*)e1->arr, (char*)e2->arr) == 0) {
			result = 1;
		}
		else {
			result = 0;
		}
	}
	else {
		return false;
	}

	if (stack_push(m->stack, TYPE_INT, &result, sizeof(int)) != RC_OK)
		return false;

	return true;
}

bool add(struct machine *m) {
	struct stack_elt *e1, *e2;
	if (stack_pop(m->stack, &e1) != RC_OK)
		return false;
	if (e1->type != TYPE_INT)
		return false;
	if (stack_pop(m->stack, &e2) != RC_OK)
		return false;
	if (e2->type != TYPE_INT)
		return false;

	int sum = *(int*)e1->arr + *(int*)e2->arr;
	if (stack_push(m->stack, TYPE_INT, &sum, sizeof(int)) != RC_OK)
		return false;

	return true;
}

bool sub(struct machine *m) {
	struct stack_elt *e1, *e2;
	if (stack_pop(m->stack, &e1) != RC_OK)
		return false;
	if (e1->type != TYPE_INT)
		return false;
	if (stack_pop(m->stack, &e2) != RC_OK)
		return false;
	if (e2->type != TYPE_INT)
		return false;

	int diff = *(int*)e1->arr - *(int*)e2->arr;
	if (stack_push(m->stack, TYPE_INT, &diff, sizeof(int)) != RC_OK)
		return false;

	return true;
}

bool _if(struct machine *m) {
	struct stack_elt *expr, *t, *f;
	char truthy[256];
	char falsy[256];
	if (stack_pop(m->stack, &expr) != RC_OK)
		return false;
	if (expr->type != TYPE_STRING)
		return false;
	if (stack_pop(m->stack, &t) != RC_OK)
		return false;
	if (t->type != TYPE_STRING)
		return false;
	if (stack_pop(m->stack, &f) != RC_OK)
		return false;
	if (f->type != TYPE_STRING)
		return false;

	strcpy(truthy, (char*)t->arr);
	strcpy(falsy, (char*)f->arr);

	struct stack_elt *res;
	if (machine_eval(m, (char*)expr->arr) != RC_OK)
		return false;
	if (stack_pop(m->stack, &res) != RC_OK)
		return false;
	if (res->type != TYPE_INT)
		return false;


	switch (*(int*)res->arr) {
	case 0:
		if (machine_eval(m, falsy) != RC_OK)
			return false;
		break;
	case 1:
		if (machine_eval(m, truthy) != RC_OK)
			return false;
		break;
	default:
		return false;
	}

	return true;
}

bool mult(struct machine *m) {
	struct stack_elt *e1, *e2;
	if (stack_pop(m->stack, &e1) != RC_OK)
		return false;
	if (e1->type != TYPE_INT)
		return false;
	if (stack_pop(m->stack, &e2) != RC_OK)
		return false;
	if (e2->type != TYPE_INT)
		return false;

	int res = (*(int*)e1->arr) * (*(int*)e2->arr);
	if (stack_push(m->stack, TYPE_INT, &res, sizeof(int)) != RC_OK)
		return false;

	return true;
}

bool eval(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;

	char expr[1024];
	strcpy(expr, (char*)elt->arr);
	if (machine_eval(m, expr) != RC_OK)
		return false;

	return true;
}

bool start(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;

	char cmd[1024];
	strcpy(cmd, (char*)elt->arr);
	system(cmd);
	return true;
}

bool change_name(struct machine *m) {
	putchar('\0'); // why does this prevent init file change-name crash?
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;

	char name[64];
	strcpy(name, (char*)elt->arr);
	interpreter_change_name(name);
	return true;
}

#ifdef LINUX
#include <link.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

bool notepad(struct machine *m) {
	system("nvim");
	return true;
}

bool chrome(struct machine *m) {
	char str[1024] = "firefox ";
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;

	if (elt->type != TYPE_STRING)
		return false;

	strcat(str, (char*)elt->arr);
	system(str);
	return true;
}

bool clear(struct machine *m) {
	system("clear");
	return true;
}

bool eval_file(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;

	struct stat stat_buf;
	int fd;
	if ((fd = open((char*)elt->arr, O_RDONLY)) == -1) {
		printf("Failed to open %s\n", (char*)elt->arr);
		return false;
	}
	if (fstat(fd, &stat_buf) == -1) {
		printf("Failed to gather info about %s\n", (char*)elt->arr);
		goto err1;
	}
	char *buf = malloc(stat_buf.st_size);
	if (buf == NULL) {
		printf("Failed to allocate file buffer for %s\n", (char*)elt->arr);
		goto err1;
	}
	if (read(fd, buf, stat_buf.st_size) == -1) {
		printf("Failed to read in file %s\n", (char*)elt->arr);
		goto err2;
	}
	buf[stat_buf.st_size - 1] = '\0';
	unsigned code;
	if ((code = machine_eval(m, buf)) != EVAL_OK) {
		printf("machine_eval fail: %d\n", code);
		goto err2;
	}

	free(buf);
	close(fd);
	return true;

err2:	free(buf);
err1:	close(fd);
	return false;
}

bool load_library(struct machine *m) {
	struct stack_elt *name, *lib;
	if (stack_pop(m->stack, &name) != RC_OK)
		return false;
	if (name->type != TYPE_STRING)
		return false;
	if (stack_pop(m->stack, &lib) != RC_OK)
		return false;
	if (lib->type != TYPE_STRING)
		return false;

	struct library *iter = m->libraries;
	unsigned i = 0;
	for (; i < m->libraries_len; ++i, ++iter)
		;

	iter->handle = dlopen((char*)lib->arr, RTLD_LAZY);
	if (iter->handle == NULL) {
		printf("Failed to open library %s\n", (char*)lib->arr);
		fprintf(stderr, "%s\n", dlerror());
		return false;
	}

	struct block *b;
	if (pool_request(m->expressions, &b, strlen((char*)name->arr) + 1) != RC_OK) {
		puts("failed to allocate for library name");
		return false;
	}
	strcpy((char*)b->arr, (char*)name->arr);
	iter->name = (char*)b->arr;
	++(m->libraries_len);

	return true;
}

bool load_primitive(struct machine *m) {
	struct stack_elt *name, *descr, *fn_name, *lib;
	if (stack_pop(m->stack, &name) != RC_OK)
		return false;
	if (name->type != TYPE_STRING)
		return false;
	if (stack_pop(m->stack, &descr) != RC_OK)
		return false;
	if (descr->type != RC_OK)
		return false;
	if (stack_pop(m->stack, &fn_name) != RC_OK)
		return false;
	if (fn_name->type != TYPE_STRING)
		return false;
	if (stack_pop(m->stack, &lib) != RC_OK)
		return false;
	if (lib->type != TYPE_STRING)
		return false;

	struct library *iter = m->libraries;
	unsigned i = 0;
	for (; i < m->libraries_len; ++i, ++iter) {
		if (strcmp(iter->name, (char*)lib->arr) == 0) {
			primitive fn = dlsym(iter->handle, (char*)fn_name->arr);
			char *err = NULL;
			if ((err = dlerror()) != NULL) {
				fprintf(stderr, "%s", err);
				return false;
			}
			if (machine_add(m, SYMBOL_PRIMITIVE, (char*)name->arr, (char*)descr->arr, (char*)fn) != RC_OK) {
				puts("failed to add primitive to machine");
				return false;
			}
			return true;
		}
	}
	
	return false;
}

bool print_libs(struct machine *m) {
	struct library *iter = m->libraries;
	unsigned i = 0;
	for (; i < m->libraries_len; ++i, ++iter) {
		printf("%s\n", iter->name);
	}
	return true;
}

bool download(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;

	char cmd[256] = "wget ";
	strcat(cmd, (char*)elt->arr);
	system(cmd);
	return true;
}

bool print_lib_symbols(struct machine *m) {
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;
	if (elt->type != TYPE_STRING)
		return false;

	struct library *iter = m->libraries;
	unsigned i = 0;
	for (; i < m->libraries_len; ++i, ++iter) {
		if (strcmp(iter->name, (char*)elt->arr) == 0) {
			struct link_map *info;
			if (dlinfo(iter->handle, RTLD_DI_LINKMAP, &info) == -1) {
				printf("Failed to load library info\n");
				return false;
			}
			char cmd[1024] = "nm -g ";
			strcat(cmd, info->l_name);
			system(cmd);
			return true;
		}
	}

	return false;
}
#endif

#ifdef WINDOWS
#include <windows.h>
bool notepad(struct machine *m) {
	system("start notepad.exe");
	return true;
}

bool chrome(struct machine *m) {
	char str[1024] = "start chrome.exe ";
	struct stack_elt *elt;
	if (stack_pop(m->stack, &elt) != RC_OK)
		return false;

	if (elt->type != TYPE_STRING)
		return false;

	strcat(str, (char*)elt->arr);
	system(str);
	return true;
}

bool clear(struct machine *m) {
	system("cls");
	return true;
}
#endif
