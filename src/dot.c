/* Very probably horribly wrong.
   Wasted space because MEM_ALIGN is a misnomer. Everything is request is rounded to 8. */
#include "dot.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MEM_ALIGN 8

retcode pool_new(struct pool **self, uint16_t id, size_t size) {
	if (size <= sizeof(struct block)) // what's the point anyways
		return 2;

	size_t align_size = size + (size % MEM_ALIGN);
	struct pool *pool;
	if ((pool = malloc(sizeof(struct pool) + align_size)) == NULL)
		return 2;

	pool->size = align_size;
	pool->id = id;
	pool->arr[0].free = true;
	pool->arr[0].pool_id = id;
	pool->arr[0].size = align_size - sizeof(struct block);
	*self = pool;

	return RC_OK;
}

retcode pool_request(struct pool *self, struct block **block, unsigned size) {
	unsigned align_size = size + (size % MEM_ALIGN);
	int diff;
	struct block *iter = self->arr;
	while (iter != NULL) {
		diff = iter->size - align_size;
		if (iter->free == true && diff >= 0) {
			iter->size = align_size;
			iter->free = false;
			*block = iter;
			if (diff > 0) {
				iter = (struct block*)((char*)(iter + 1) + align_size);
				iter->free = true;
				iter->pool_id = self->id;
				iter->size = diff - sizeof(struct block);
			}
			return RC_OK;
		} else {
			iter = (struct block*)((char*)(iter + 1) + iter->size);
		}
	}

	return 1;
}

retcode pool_release(struct pool *self, struct block *block) {
	struct block *iter = self->arr;	
	size_t count = 0;
	for (; count < self->size; count += iter->size + sizeof(struct block), iter = (struct block*)((char*)(iter + 1) + iter->size)) {
		if (iter == block) {
			iter->free = true;
			return RC_OK;
		}
	}

	return 1;
}

void pool_coalesce(struct pool *self) {
	struct block *iter = self->arr;
	struct block *prev;
	size_t count = 0;
	do {
		count += iter->size + sizeof(struct block);
		if (count >= self->size)
			break;

		prev = iter;
		iter = (struct block*)((char*)(iter + 1) + iter->size);

		if (prev->free == true && iter->free == true) {
			prev->size += sizeof(struct block) + iter->size;
			iter = prev;
		}
	} while (1); // fuck me
}

retcode pool_free(struct pool *self) {
	if (self == NULL)
		return 1;

	free(self);

	return RC_OK;
}

void pool_print(struct pool *self) {
	struct block *iter = self->arr;
	size_t count = 0;
	for (; count < self->size; count += iter->size + sizeof(struct block), iter = (struct block*)((char*)(iter + 1) + iter->size)) {
		printf("#: %zu\tFree:%d\tPool id:%d\tSize:%d\t\n", count, iter->free, iter->pool_id, iter->size);
	}
	printf("\n");
}

retcode stack_new(struct stack **self, size_t capacity) {
	size_t align_size = capacity + (capacity % MEM_ALIGN);
	struct stack *stack;
	if ((stack = malloc(sizeof(struct stack) + align_size)) == NULL)
		return 1;

	stack->capacity = capacity;
	stack->size = 0;
	stack->count = 0;
	*self = stack;

	return RC_OK;
}

retcode stack_push(struct stack *self, uint32_t type, void *buf, uint32_t len) {
	switch (type) {
	case TYPE_INT:
	case TYPE_STRING:
	case TYPE_FLOAT:
		break;
	default:
		return 1;
	}

	uint32_t align_len = len + (len % MEM_ALIGN);
	size_t new_size = self->size + sizeof(struct stack_elt) + align_len;
	if (new_size > self->capacity)
		return 2;

	struct stack_elt *elt = (struct stack_elt*)(((char*)self->arr) + self->size);
	elt->type = type;
	elt->size = align_len;
	memcpy(elt->arr, buf, len);

	++self->count;
	self->size = new_size;

	return RC_OK;
}

retcode stack_pop(struct stack *self, struct stack_elt **elt) {
	if (self->count <= 0)
		return RC_ERR;

	unsigned i = 0;
	struct stack_elt *iter = self->arr;
	while (i < self->count - 1) {
		iter = (struct stack_elt*)(((char*)(iter + 1)) + iter->size);
		++i;
	}

	*elt = iter;
	--self->count;
	self->size -= iter->size + sizeof(struct stack_elt);

	return RC_OK;
}

retcode stack_free(struct stack *self) {
	if (self == NULL)
		return 1;

	free(self);

	return RC_OK;
}

void stack_print(struct stack *self) {
	printf("Capacity: %zu\tSize: %zu\t\n", self->capacity, self->size);
	unsigned i = 0;
	struct stack_elt *iter = self->arr;
	printf("Type\tValue\tSize\n");
	while (i < self->count) {
		switch (iter->type) {
		case TYPE_INT:
			printf("int\t%d\t%d\n", *(int*)iter->arr, iter->size);
			break;
		case TYPE_STRING:
			printf("string\t%s\t%d\n", (char*)iter->arr, iter->size);
			break;
		case TYPE_FLOAT:
			printf("float\t%.2f\t%d\n", *(float*)iter->arr, iter->size);
			break;
		default:
			printf("unknown type\n");
		}
		iter = (struct stack_elt*)(((char*)(iter + 1)) + iter->size);
		++i;
	}
}

retcode symbol_table_new(struct symbol_table **self, size_t capacity) {
	struct symbol_table *table = malloc(sizeof(struct symbol_table) + (sizeof(struct symbol) * capacity));
	if (table == NULL)
		return 1;

	table->capacity = capacity;
	table->count = 0;
	*self = table;

	return RC_OK;
}

retcode symbol_table_add(struct symbol_table **self, uint32_t type, char *name, char *description, char *data) {
	struct symbol_table *table = *self;
	if (table->count + 1 >= table->capacity) {
		struct symbol_table *new = malloc(sizeof(struct symbol_table) + (sizeof(struct symbol) + (table->capacity * SYMBOL_TABLE_GROWTH_FACTOR)));
		if (new == NULL)
			return RC_ERR;

		memcpy(new, table, sizeof(struct symbol_table) + (sizeof(struct symbol) * table->count));
		struct symbol_table *tmp = table;
		table = new;
		free(tmp);
	}

	struct symbol *last = table->symbols + table->count;
	last->type = type;
	last->use = 0;
	last->name = name;
	last->description = description;
	last->data = data;
	++(table->count);
	*self = table;

	return RC_OK;
}

retcode symbol_table_search(struct symbol_table *self, char *name, char **data) {
	unsigned i = 0;
	struct symbol *iter = self->symbols;
	for (; i < self->count; ++i, ++iter) {
		if (strcmp(iter->name, name) == 0) {
			*data = iter->data;
			++(iter->use);
			if (iter->type == SYMBOL_PRIMITIVE)
				return SYMBOL_SEARCH_PRIMITIVE;
			else
				return SYMBOL_SEARCH_EXPRESSION;
		}
	}

	return SYMBOL_SEARCH_NOT_FOUND;
}

void symbol_table_sort(struct symbol_table *self) {
}

retcode symbol_table_free(struct symbol_table *self) {
	if (self == NULL)
		return 1;

	free(self);

	return RC_OK;
}

#include <unistd.h>
#include <limits.h>
retcode machine_init(struct machine *self, struct machine_config *config) {
	retcode code = RC_OK;
	if (pool_new(&self->names, 0, config->namesPoolSize) != RC_OK) {
		code = 1;
		goto err1;
	}

	if (pool_new(&self->descriptions, 1, config->descriptionsPoolSize) != RC_OK) {
		code = 2;
		goto err2;
	}

	if (pool_new(&self->expressions, 2, config->expressionsPoolSize) != RC_OK) {
		code = 3;
		goto err3;
	}

	if (stack_new(&self->stack, config->stackSize) != RC_OK) {
		code = 4;
		goto err4;
	}

	if (symbol_table_new(&self->table, config->symbolTableCapacity) != RC_OK) {
		code = 5;
		goto err5;
	}

	unsigned i = 0;
	struct symbol *iter = config->primitives;
	for (; i < config->primitives_len; ++i, ++iter) {
		if (symbol_table_add(&self->table, iter->type,
		                                   iter->name,
		                                   iter->description,
		                                   iter->data) != RC_OK)
		{
			puts("failed to add primitives");
			goto err5;
		}
	}

	self->libraries_len = 0;

	char init[256];
	strcpy(init, config->initFile);
	strcat(init, " eval-file.");
	if (machine_eval(self, init) != EVAL_OK) {
		printf("error evaluating init file\n");
	}

	return RC_OK;

err5:	stack_free(self->stack);
err4:	pool_free(self->expressions);
err3:	pool_free(self->descriptions);
err2:	pool_free(self->names);
err1:	return code;
}

void machine_free(struct machine *self) {
	pool_free(self->names);
	pool_free(self->descriptions);
	pool_free(self->expressions);
	stack_free(self->stack);
	symbol_table_free(self->table);

#ifdef LINUX
#include <dlfcn.h>

	struct library *iter = self->libraries;
	unsigned i = 0;
	for (; i < self->libraries_len; ++i, ++iter) {
		dlclose(iter->handle);
	}
#endif
}

retcode machine_add(struct machine *self, uint32_t type, char *name, char *descr, char *expr) {
	struct block *n, *d, *e;
	if (pool_request(self->names, &n, strlen(name) + 1) != RC_OK) {
		puts("failed to allocate new name");
		return RC_ERR;
	}
	strcpy((char*)n->arr, name);
	if (pool_request(self->descriptions, &d, strlen(descr) + 1) != RC_OK) {
		puts("failed to allocate new description");
		return RC_ERR;
	}
	strcpy((char*)d->arr, descr);

	switch (type) {
	case SYMBOL_EXPRESSION:
		if (pool_request(self->expressions, &e, strlen(expr) + 1) != RC_OK) {
			puts("failed to allocate new expression");
			return RC_ERR;
		}
		strcpy((char*)e->arr, expr);
		break;
	case SYMBOL_PRIMITIVE:
		if (pool_request(self->expressions, &e, sizeof(void*)) != RC_OK) {
			puts("failed to allocate new expression");
			return RC_ERR;
		}
		//(*(primitive)expr)(self);
		//printf("Loaded %p\n", expr);
		//void *test;
		//memcpy(&test, &expr, sizeof(void*));
		//printf("Test %p\n", test);
		//printf("&expr: %p\nexpr: %p\n", &expr, expr);
		memcpy(e->arr, &expr, sizeof(void*));
		//printf("e->arr: %p\n", *(void**)e->arr);
		if (symbol_table_add(&self->table, type, (char*)n->arr, (char*)d->arr, (char*)(*(void**)e->arr)) != RC_OK)
			return RC_ERR;
		else
			return RC_OK;
		break;
	default:
		puts("machine add: unknown type");
		return RC_ERR;
	}
	
	if (symbol_table_add(&self->table, type, (char*)n->arr, (char*)d->arr, (char*)e->arr) != RC_OK) {
		puts("failed to add to symbol table");
		return RC_ERR;
	}

	return RC_OK;
}

retcode machine_eval(struct machine *m, char *in) {
	char out[MAX_TOKEN_LENGTH];
	char *head = out;
	char *expr;
	char ch;
	int brace = 0;

start: // Fresh as if no input has been read yet
	ch = *in;
	if (isspace(ch)) {
		++in;
		goto start;
	}
	else if (isdigit(ch)) {
		*head = ch;
		++head;
		++in;	
		goto number;
	}
	else if (ch == '-') {
		*head = ch;
		++head;
		++in;
		goto maybe_negative;
	}
	else if (ch == '{') {
		++brace;
		++in;
		goto brace;
	}
	else if (ch == '.') {
		++in;
		goto maybe_apply;
	}
	else if (ch == '\0') {
		return EVAL_MUST_END_IN_DOT;
	}
	else {
		*head = ch;
		++head;
		++in;
		goto normal;
	}
normal: // We've encountered one alphanumeric character that's not '"' from start state
	ch = *in;
	if (isspace(ch)) {
		*head = '\0';
		++head;
		stack_push(m->stack, TYPE_STRING, out, head - out);
		head = out;
		++in;
		goto start;
	}
	else if (ch == '.') {
		++in;
		goto maybe_apply;
	}
	else if (ch == '\0') {
		return EVAL_MUST_END_IN_DOT;
	}
	else {
		*head = ch;	
		++head;
		++in;
		goto normal;
	}
maybe_apply: // We came from normal or number modes and are checking to see if '.' is at the end of token
	ch = *in;
	if (isspace(ch)) {
		*head = '\0';
		switch (symbol_table_search(m->table, out, &expr)) {
		case SYMBOL_SEARCH_PRIMITIVE:
			if (!((primitive)expr)(m))
				return EVAL_PRIMITIVE_EXCEPTION;
			break;
		case SYMBOL_SEARCH_EXPRESSION:
			if (machine_eval(m, expr) != EVAL_OK)
				return EVAL_EXPRESSION_EXCEPTION;
			break;
		case SYMBOL_SEARCH_NOT_FOUND:
			return EVAL_SYMBOL_UNDEFINED;
		}
		head = out;
		++in;
		goto start;
	}
	else if (ch == '\0') {
		*head = '\0';
		switch (symbol_table_search(m->table, out, &expr)) {
		case SYMBOL_SEARCH_PRIMITIVE:
			if (!((primitive)expr)(m))
				return EVAL_PRIMITIVE_EXCEPTION;
			break;
		case SYMBOL_SEARCH_EXPRESSION:
			if (machine_eval(m, expr) != EVAL_OK)
				return EVAL_EXPRESSION_EXCEPTION;
			break;
		case SYMBOL_SEARCH_NOT_FOUND:
			printf("Undefined symbol: %s\n", out);
			return EVAL_SYMBOL_UNDEFINED;
		}
		return EVAL_OK;
	}
	else {
		*head = '.';
		++head;
		*head = *in;
		++head;
		++in;
		goto normal;
	}
brace:
	ch = *in;
	if (ch == '}') {
		--brace;
		if (brace == 0) {
			*head = '\0';
			++head;
			stack_push(m->stack, TYPE_STRING, out, head - out);
			head = out;
			++in;
			goto start;
		}
		else {
			*head = ch;
			++head;
			++in;
			goto brace;
		}
	}
	else if (ch == '{') {
		++brace;
		*head = ch;
		++head;
		++in;
		goto brace;
	}
	else if (ch == '\\') {
		++in;
		*head = *in;
		++head;
		++in;
		goto brace;
	}
	else if (ch == '\0') {
		return EVAL_MISSING_CLOSING_BRACE;
	}
	else {
		*head = ch;
		++head;
		++in;
		goto brace;
	}
number: // we came from start where a digit was the first encountered character of token
	ch = *in;
	if (isspace(ch)) {
		*head = '\0';
		*(int*)out = atoi(out);
		stack_push(m->stack, TYPE_INT, out, sizeof(int));
		head = out;
		++in;
		goto start;
	}
	else if (isdigit(ch)) {
		*head = ch;
		++head;
		++in;
		goto number;
	}
	else if (ch == '\0') {
		return EVAL_MUST_END_IN_DOT;
	}
	else if (ch == '.') {
		++in;
		goto maybe_float_or_apply;
	}
maybe_float_or_apply: // We came from number where we encountered a '.'
	ch = *in;
	if (isspace(ch)) {
		*head = '\0';
		switch (symbol_table_search(m->table, out, &expr)) {
		case SYMBOL_SEARCH_PRIMITIVE:
			if (!((primitive)expr)(m))
				return EVAL_PRIMITIVE_EXCEPTION;
			break;
		case SYMBOL_SEARCH_EXPRESSION:
			if (machine_eval(m, expr) != EVAL_OK)
				return EVAL_EXPRESSION_EXCEPTION;
			break;
		case SYMBOL_SEARCH_NOT_FOUND:
			return EVAL_SYMBOL_UNDEFINED;
		}
		head = out;
		++in;
		goto start;
	}
	else if (isdigit(ch)) {
		*head = '.';
		++head;
		*head = ch;
		++head;
		++in;
		goto maybe_float;
	}
	else if (ch == '\0') {
		*head = '\0';
		switch (symbol_table_search(m->table, out, &expr)) {
		case SYMBOL_SEARCH_PRIMITIVE:
			if (!((primitive)expr)(m))
				return EVAL_PRIMITIVE_EXCEPTION;
			break;
		case SYMBOL_SEARCH_EXPRESSION:
			if (machine_eval(m, expr) != EVAL_OK)
				return EVAL_EXPRESSION_EXCEPTION;
			break;
		case SYMBOL_SEARCH_NOT_FOUND:
			return EVAL_SYMBOL_UNDEFINED;
		}
		return EVAL_OK;
	}
	else {
		*head = '.';
		++head;
		*head = ch;
		++head;
		++in;
		goto normal;
	}
maybe_float:
	ch = *in;
	if (isspace(ch)) {
		*head = '\0';
		*(float*)out = atof(out);
		stack_push(m->stack, TYPE_FLOAT, out, sizeof(float));
		head = out;
		++in;
		goto start;
	}
	else if (isdigit(ch)) {
		*head = ch;
		++head;
		++in;
		goto maybe_float;
	}
	else if (ch == '.') {
		++in;
		goto maybe_apply;
	}
	else {
		*head = ch;
		++head;
		++in;
		goto normal;
	}	
maybe_negative: // We came from start where '-' was the first encountered character of token
	ch = *in;
	if (isspace(ch)) {
		*head = '\0';
		++head;
		stack_push(m->stack, TYPE_STRING, out, head - out);
		head = out;
		++in;
		goto start;
	}
	else if (isdigit(ch)) {
		*head = ch;
		++head;
		++in;
		goto number;
	}
	else if (ch == '\0') {
		return EVAL_MUST_END_IN_DOT;
	}
	else {
		*head = ch;
		++head;
		++in;
		goto normal;
	}
}
