#include "interpreter.h"
#include "lolopt.h"
#include "primitives.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool g_quit = false;
char g_name[64] = "dotty >> ";

struct options g_opts = {"--namesPoolSize=",
                         "--descriptionsPoolSize=",
			 "--expressionsPoolSize=",
			 "--stackSize=",
			 "--symbolTableCapacity=",
			 "--initFile="};

retcode interpreter_start(int argc, char **argv) {
	struct machine_config config;
	config.namesPoolSize = 1024;
	config.descriptionsPoolSize = 1024;
	config.expressionsPoolSize = 1024;
	config.stackSize = 1024;
	config.symbolTableCapacity = 32;
	config.primitives = primitives;
	config.primitives_len = sizeof(primitives) / sizeof(primitives[0]);
	char init[256];
	strcpy(init, getenv("HOME"));
	strcat(init, "/init.dot");
	config.initFile = init;

	char **opt = NULL;
	if ((opt = lolopt(opt, g_opts.namesPoolSize, argc, argv)) != NULL) {
		config.namesPoolSize = atol(*opt + strlen(g_opts.namesPoolSize));
	}
	opt = NULL;
	if ((opt = lolopt(opt, g_opts.descriptionsPoolSize, argc, argv)) != NULL) {
		config.descriptionsPoolSize = atol(*opt + strlen(g_opts.descriptionsPoolSize));
	}
	opt = NULL;
	if ((opt = lolopt(opt, g_opts.expressionsPoolSize, argc, argv)) != NULL) {
		config.expressionsPoolSize = atol(*opt + strlen(g_opts.expressionsPoolSize));
	}
	opt = NULL;
	if ((opt = lolopt(opt, g_opts.stackSize, argc, argv)) != NULL) {
		config.stackSize = atol(*opt + strlen(g_opts.stackSize));
	}
	opt = NULL;
	if ((opt = lolopt(opt, g_opts.symbolTableCapacity, argc, argv)) != NULL) {
		config.symbolTableCapacity = atol(*opt + strlen(g_opts.symbolTableCapacity));
	}
	opt = NULL;
	if ((opt = lolopt(opt, g_opts.symbolTableCapacity, argc, argv)) != NULL) {
		config.symbolTableCapacity = atol(*opt + strlen(g_opts.symbolTableCapacity));
	}
	opt = NULL;
	if ((opt = lolopt(opt, g_opts.initFile, argc, argv)) != NULL) {
		config.initFile = *opt + strlen(g_opts.initFile);
	}

	struct machine machine;
	if (machine_init(&machine, &config) != RC_OK) {
		puts("Failed to initialize machine");
		return RC_ERR;
	}

	char in[1024];
	while (!g_quit) {
		printf("%s", g_name);
		if (fgets(in, 1024, stdin) == NULL) {
			g_quit = true;
			puts("error reading in");
		}
		in[strcspn(in, "\n")] = 0;
		machine_eval(&machine, in);
	}

	machine_free(&machine);

	return RC_OK;
}

retcode interpreter_change_name(char *str) {
	if (strlen(str) < 64) {
		strcpy(g_name, str);
		return RC_OK;
	}
	else {
		return RC_ERR;
	}
}

retcode interpreter_quit() {
	g_quit = true;
	return RC_OK;
}
