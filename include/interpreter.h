#ifndef INTERPRETER_H
#define INTERPRETER_H
#include "dot.h"

struct options {
	char *namesPoolSize;
	char *descriptionsPoolSize;
	char *expressionsPoolSize;
	char *stackSize;
	char *symbolTableCapacity;
	char *initFile;
};
retcode interpreter_start(int argc, char **argv);
retcode interpreter_change_name(char *str);
retcode interpreter_quit();

#endif
