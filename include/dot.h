#ifndef DOT_H
#define DOT_H
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define RC_ERR 0
#define RC_OK 1
typedef unsigned retcode;

struct block {
	bool free;
	uint16_t pool_id;
	uint32_t size;
	void *arr[];
};

struct pool {
	uint16_t id;
	size_t size;	
	struct block arr[];
};

retcode pool_new(struct pool **self, uint16_t id, size_t size);
retcode pool_request(struct pool *self, struct block **block, unsigned size);
retcode pool_release(struct pool *self, struct block *block);
void pool_coalesce(struct pool *self);
retcode pool_free(struct pool *self);
void pool_print(struct pool *self);

#define TYPE_INT 0
#define TYPE_STRING 1
#define TYPE_FLOAT 3

struct stack_elt {
	uint32_t type;
	uint32_t size;
	void *arr[];
};

struct stack {	
	size_t capacity;
	size_t size;
	unsigned count;
	struct stack_elt arr[];
};

retcode stack_new(struct stack **self, size_t capacity);
retcode stack_push(struct stack *self, uint32_t type, void *buf, uint32_t len);
retcode stack_pop(struct stack *self, struct stack_elt **elt);
retcode stack_free(struct stack *self);
void stack_print(struct stack *self);

struct library {
	char *name;
	void *handle;
};

#define SYMBOL_PRIMITIVE 0
#define SYMBOL_EXPRESSION 1

struct symbol {
	uint32_t type;
	uint32_t use;
	char *name;
	char *description;
	char *data;
};

#define SYMBOL_TABLE_GROWTH_FACTOR 2

struct symbol_table {
	size_t capacity;
	unsigned count;
	struct symbol symbols[];
};

#define SYMBOL_SEARCH_NOT_FOUND 0
#define SYMBOL_SEARCH_PRIMITIVE 1
#define SYMBOL_SEARCH_EXPRESSION 2

retcode symbol_table_new(struct symbol_table **self, size_t capacity);
retcode symbol_table_add(struct symbol_table **self, uint32_t type, char *name, char *description, char *data);
retcode symbol_table_search(struct symbol_table *self, char *name, char **data);
void symbol_table_sort(struct symbol_table *self);
retcode symbol_table_free(struct symbol_table *self);

#define MAX_TOKEN_LENGTH 256

#define EVAL_UNKNOWN 0
#define EVAL_OK 1
#define EVAL_MUST_END_IN_DOT 2
#define EVAL_PRIMITIVE_EXCEPTION 3
#define EVAL_EXPRESSION_EXCEPTION 4
#define EVAL_SYMBOL_UNDEFINED 5
#define EVAL_MISSING_CLOSING_QUOTE 6
#define EVAL_MISSING_CLOSING_BRACE 7

struct machine {
	struct pool *names;
	struct pool *descriptions;
	struct pool *expressions;
	struct stack *stack;
	struct symbol_table *table;
	struct library libraries[8];
	unsigned libraries_len;
};

typedef retcode (*primitive)(struct machine*);

struct machine_config {
	size_t namesPoolSize;
	size_t descriptionsPoolSize;
	size_t expressionsPoolSize;
	size_t stackSize;
	size_t symbolTableCapacity;
	char *initFile;
	struct symbol *primitives;
	unsigned primitives_len;
};

retcode machine_init(struct machine *self, struct machine_config *config);
retcode machine_add(struct machine *self, uint32_t type, char *name, char *descr, char *expr);
void machine_free(struct machine *self);
retcode machine_eval(struct machine*, char *in);

#endif
