#ifndef PRIMITIVES_H
#define PRIMITIVES_H
#include "dot.h"

bool print(struct machine*);
bool print_int(struct machine*);
bool print_float(struct machine*);
bool define(struct machine*);
bool print_stack(struct machine*);
bool quit(struct machine*);
bool print_symbols(struct machine*);
bool clear(struct machine*);
bool loop(struct machine*);
bool notepad(struct machine*);
bool chrome(struct machine*);
bool nil(struct machine*);
bool equals(struct machine*);
bool add(struct machine*);
bool sub(struct machine*);
bool _if(struct machine*);
bool mult(struct machine*);
bool eval(struct machine*);
bool start(struct machine*);
bool change_name(struct machine*);
bool eval_file(struct machine*);
bool load_library(struct machine*);
bool load_primitive(struct machine*);
bool print_libs(struct machine*);
bool download(struct machine*);
bool print_lib_symbols(struct machine*);

extern bool g_quita;
extern struct symbol primitives[26];

#endif
