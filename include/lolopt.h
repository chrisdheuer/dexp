#ifndef LOLOPT_H
#define LOLOPT_H
/* *FOUR TYPES OF FORMAT ARGUMENTS*
 * There are four types of arguments, and thus four format forms you
 * can pass to this getopt implementation.
 *
 * The 1st is the empty string. This will give you all of the arguments
 * directly after the program invocation. Example:
 * 'myprogram arg1 arg2 -f something --debug'
 *                      |
 *                      v
 * char **opt == NULL;
 * while ((opt = lolopt(opt, "", argc, argv)) != NULL) {
 *      printf("%s ", *opt);
 * }
 *                      |
 *                      v
 * 'arg1 arg2 '
 *
 * The 2nd is and option prefixed by '-'. This will give you all arguments
 * that come after the specified option. Example:
 * 'myprogram arg1 -link getopt.o main.o -d arg2 -link awesome.o
 *                      |
 *                      v
 * char **opt == NULL;
 * while ((opt = lolopt(opt, "-link", argc, argv)) != NULL) {
 *     printf("%s ", *opt);
 * }
 *                      |
 *                      v
 * 'getopt.o main.o awesome.o '
 *
 * The 3rd is an option prefixed by '--'. This type of option takes no arguments,
 * so getopt will return NULL if not found, and pointer to the argument in argv
 * if it is. Example:
 * 'myprogram somearg -debug getopt.o --version'
 *                      |
 *                      v
 * char **opt == NULL;
 * if ((opt = lolopt(opt, "--version", argc, argv)) != NULL) {
 *     printf("found!");
 * }
 *                      |
 *                      v
 * 'found!'
 *
 * The 4th is an option prefixed with '--' and suffixed with '='. This type of option
 * can only take one option that comes directly after the '='. Example that demonstrates
 * usage and also a gotcha:
 * 'myprogram anargy --std=c11 -link getopt.o --version --std=c99'
 *                      |
 *                      v
 * char **opt = NULL;
 * while ((opt = lolopt(opt, "--std=", argc, argv)) != NULL) {
 * 	printf("%s ", *opt);
 * }
 *                      |
 *                      v
 * '--std=c11 --std=c99 '
 *
 * *SET OPT PARAMETER TO NULL BEFORE USING LOLOPT*
 * If 'opt' is set to NULL then lolopt will start scanning argv from the beginning.
 * Otherwise getopt continues where it left off. This is to avoid keeping any state
 * behind the scenes. The state is returned to you. */
char **lolopt(char **opt, char *format, int argc, char **argv);
#endif
